#!/bin/bash

wikicodes=($WIKICODES)

envsubst < tinyows.xml.head > /etc/tinyows.xml

for wikicode in "${wikicodes[@]}"
do
    export WIKICODE=${wikicode}
    envsubst < tinyows.xml.body >> /etc/tinyows.xml
done

envsubst < tinyows.xml.tail >> /etc/tinyows.xml

# spawn-fcgi -U www-data -G www-data -F $(nproc) -s /var/run/ows.socket -- /usr/lib/cgi-bin/ows
/etc/init.d/supervisor start
supervisorctl start all

nginx -g "daemon off;"
