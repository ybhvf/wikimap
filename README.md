# Wikimap

Wikimap is a map of every geotagged article in the largest language-specific
Wikipedias ([wikimap.wiki](https://wikimap.wiki/)).

Wikimap has a few components:

- [Wikiscan](wikiscan), a python tool for scraping and processing
  Wikidata/Wikipedia dumps
- [Wikimap](wikimap), the interactive map frontend
- [Mapnik](mapnik), used for rendering and serving tiles
- [TinyOWS](tinyows), used for geospatial queries
- [PostGIS](postgis), used for storing geospatial data

## Building & Deployment

All components are containerized, and kubernetes/kustomize are used to deploy
the full project. You will likely need to write your own kustomization.yaml
base to tweak various settings (secrets, ingress URLs, TLS, image names, etc).

## Other

- [TODOs](TODO.md)
- [License](LICENSE.md)
