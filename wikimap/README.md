# Wikimap

Wikimap is the interactive map frontend. Important buzzwords here are
OpenLayers, TypeScript, VueJS, and Buefy.

# Local Testing

You can locally run and (somewhat) test wikimap with `yarn dev`.

# Building

```bash
yarn build
podman build . -t KUBE_PREFIX/wikimap
```
