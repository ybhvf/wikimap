# Wikiscan

*Wikiscan* is a python tool used for preparing the PostGIS database, and
populating it from Wikidata/Wikipedia dumps. Wikiscan supports Wikidata JSON
files, Wikipedia XML files, and Wikipedia SQL files (via an intermediary
database). Wikiscan is by far the most resource intensive component of wikimap.

## Usage

Run `poetry run wikiscan --help` for detailed usage info. [run.sh](run.sh)
is used for regular scheduled imports, and is a good example of how wikiscan is
typically used.

## Building

```bash
poetry build
podman build . -t KUBE_PREFIX/wikiscan
```
