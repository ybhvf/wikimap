#!/usr/bin/perl -w
use POSIX;
use utf8;
use strict;
use warnings;

my $id = '"?[-_\w]+"?';
my $word = '[^,\s]+';

# exit on ^c
$SIG{INT} = $SIG{TERM} = sub { exit; };

while (<>) {

if (/INSERT\s+INTO/i) {
    # unpack('H*', $1)
    s/'((?:[^'\\]++|\\.)*+)'(?=[),])/E'$1'/g; # '...' -> E'...'
    s!\\\\!\\\\\\\\!g;                        # \\ -> \\\\

    $_ =~ m/INSERT\s+INTO\s+`?(${id})`?\s+VALUES\s*(.*);/i;
    print STDOUT "INSERT INTO \"$1\" VALUES $2;\n";

    next;
}

s/`/"/g;
if (/CREATE\s+TABLE\s+(${id})/i) {
    my $table = $1;
    print STDOUT "DROP TABLE IF EXISTS $table CASCADE;\n";
    print STDOUT "CREATE UNLOGGED TABLE $table (\n";

    # process the body of CREATE TABLE (...)
    while (<>) {
        s/`/"/g;

        # end of CREATE TABLE (...)
        if (/\).*;/i) {
            print STDOUT ");\n";
            last;
        }

        if (/PRIMARY\s+KEY/i) {
            s/\).*,/\)/;
            print STDOUT $_;
            next;
        }

        # skip non-primary keys
        if (/^\s*(UNIQUE\s+)?KEY/i) {
            next;
        }

        if (/^\s*${id}/i) {
            my $etc = '(\([^\)]+\))?';

            # convert types
            s/\s(TINYINT|SMALLINT)${etc}/ smallint/i;
            s/\s(MEDIUMINT|INT)${etc}/ integer/i;
            s/\sBIGINT${etc}/ bigint/i;

            s/\ssmallint\s+unsigned/ integer/i;
            s/\sinteger\s+unsigned/ bigint/i;

            s/\s(VARBINARY|BINARY)${etc}/ bytea/i;
            s/\sDOUBLE${etc}/ double precision/i;
            s/\sDECIMAL${etc}/ decimal/i;
            s/\sENUM${etc}/ varchar/i;

            # drop constraints
            s/NOT NULL//i;
            s/UNSIGNED//i;
            s/AUTO_INCREMENT//i;
            s/DEFAULT\s*${word}//i;
            s/ON\s+UPDATE\s+${word}//i;

            print STDOUT $_;
        }
    }
}

}
