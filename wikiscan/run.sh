#!/bin/bash
# TODO: this should be an environment variable
MIRROR="https://dumps.wikimedia.org"
PSQL_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${POSTGRES_DB}"

cd /scratch

# verbose wget alias
wgetv="wget --progress=dot -e dotbytes=10M"

# 1. Wikidata
if [ -n "$IMPORT_WIKIDATA" ]; then
    echo "loading wikidata"

    psql -a $PSQL_URL <<EOF
DROP TABLE IF EXISTS wikidata;
CREATE TABLE wikidata (site TEXT, title TEXT, lat REAL, lon REAL, globe TEXT);
EOF

    echo "loading wikidata"
    $wgetv ${MIRROR}/wikidatawiki/entities/latest-all.json.bz2 -O - | lbzip2 -d -c |
        grep P625 | grep latitude | sed 's#,$\|\[$\|\]$)##' |
        jq -r -c 'try ([[.sitelinks[] // error], [.claims.P625[0].mainsnak.datavalue.value // error]] | combinations | [(.[0] | .site, .title), (.[1] | .latitude, .longitude, .globe)]) catch empty | @csv' |
        psql -q $PSQL_URL -c "\copy wikidata FROM stdin WITH (FORMAT csv)"

    psql -a $PSQL_URL <<EOF
UPDATE wikidata SET globe = 'earth' WHERE globe = 'http://www.wikidata.org/entity/Q2';

CREATE INDEX ON wikidata(site);
CREATE INDEX ON wikidata(title);
VACUUM ANALYZE wikidata;
EOF

fi

# 2. Wikipedia
# parallel import, serial processing
for wiki in $WIKICODES
do (
    # wikimedia disallows simultaneous downloads
    (flock -x 9
        echo "loading $wiki"

        for table in page categorylinks geo_tags
        do
            FILE="${wiki}-latest-${table}.sql.gz"
            $wgetv "${MIRROR}/${wiki}/latest/${FILE}"
        done
    ) 9>/scratch/wget.flock

    for table in page categorylinks geo_tags
    do (
        FILE="${wiki}-latest-${table}.sql.gz"
        gzip -dc ${FILE} |
            uconv --callback skip |
            sed "s#\`${table}\`#\`${wiki}_${table}\`#" |
            mysql2pgsql.pl |
            psql -q $PSQL_URL
        rm ${wiki}-latest-${table}.sql.gz
    )& done

    wait

    (flock -x 10
        psql -a $PSQL_URL <<EOF
/******************************************************************************/
/* Prepare page/categorylinks/geo_tags */

/* These columns are all unicode */
ALTER TABLE ${wiki}_page ALTER COLUMN page_title TYPE TEXT USING convert_from(page_title, 'UTF8');
ALTER TABLE ${wiki}_categorylinks ALTER COLUMN cl_to TYPE TEXT USING convert_from(cl_to, 'UTF8');
ALTER TABLE ${wiki}_geo_tags ALTER COLUMN gt_globe TYPE TEXT USING convert_from(gt_globe, 'UTF8');

/* Create indices on important columns */
DELETE FROM ${wiki}_page WHERE page_namespace != 0; /* CREATE INDEX ON page(page_namespace); */
CREATE INDEX ON ${wiki}_page(page_id);
CREATE INDEX ON ${wiki}_page(page_title);

CREATE INDEX ON ${wiki}_categorylinks(cl_from);

DELETE FROM ${wiki}_geo_tags WHERE gt_primary != 1; /* CREATE INDEX ON geo_tags(gt_primary); */
CREATE INDEX ON ${wiki}_geo_tags(gt_page_id);

/* Replace underscores in page names */
UPDATE ${wiki}_page SET page_title=replace(page_title, '_', ' ')
    WHERE page_title LIKE '%#_%' ESCAPE '#';

UPDATE ${wiki}_categorylinks SET cl_to=replace(cl_to, '_', ' ')
    WHERE cl_to LIKE '%#_%' ESCAPE '#';

VACUUM ANALYZE ${wiki}_page;
VACUUM ANALYZE ${wiki}_categorylinks;
VACUUM ANALYZE ${wiki}_geo_tags;

/******************************************************************************/
/* (page, categorylinks, geo_tags, wikidata) -> xxwiki */

DROP TABLE IF EXISTS $wiki;
CREATE UNLOGGED TABLE $wiki (title TEXT UNIQUE, lon REAL, lat REAL, globe TEXT, categories TEXT);

INSERT INTO $wiki
    SELECT page_title, gt_lon, gt_lat, gt_globe,
            '#' || (SELECT string_agg(cl_to, '#') FROM ${wiki}_categorylinks
                    WHERE cl_from = page_id) || '#'
        FROM ${wiki}_page INNER JOIN ${wiki}_geo_tags
            ON (page_id = gt_page_id);
     /* WHERE page.page_namespace = 0 AND gt_primary = 1; */

INSERT INTO $wiki
    SELECT title, lon, lat, globe,
            '#' || (SELECT string_agg(cl_to, '#')
                    FROM ${wiki}_categorylinks INNER JOIN ${wiki}_page ON (cl_from = page_id)
                    WHERE page_title = title AND site = '$wiki') || '#'
        FROM wikidata
        WHERE site = '$wiki'
    ON CONFLICT DO NOTHING;

DROP TABLE ${wiki}_page;
DROP TABLE ${wiki}_categorylinks;
DROP TABLE ${wiki}_geo_tags;

/* remove non-Earth entries */
DELETE FROM $wiki WHERE globe != 'earth';

/* encode lat/lon using as gis data */
ALTER TABLE $wiki ADD COLUMN location geometry(Point, 4326);
UPDATE $wiki SET location = ST_SetSRID(ST_MakePoint(lon, lat), 4326);

CREATE INDEX ${wiki}_gidx ON $wiki USING GIST (location);
CLUSTER $wiki USING ${wiki}_gidx;
VACUUM ANALYZE $wiki;

/******************************************************************************/
/* xxwiki -> map_xxwiki */

/* calculate visual densities (very slow!) */
DROP TABLE IF EXISTS map_${wiki}_new;
CREATE TABLE map_${wiki}_new AS
    SELECT a.globe,
           a.location,
           a.title,
           a.categories,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/128.0))
            AS density_128,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/64.0))
            AS density_64,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/32.0))
            AS density_32,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/16.0))
            AS density_16,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/8.0))
            AS density_8,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/4.0))
            AS density_4,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0/2.0))
            AS density_2,

           (SELECT Count(b) FROM $wiki AS b WHERE
                ST_DWithin(a.location, b.location, 1.0))
            AS density_1
    FROM $wiki AS a;

/* used by mapnik to ensure consistent ordering during render */
ALTER TABLE map_${wiki}_new ADD COLUMN id SERIAL PRIMARY KEY;

/* index new map table */
CREATE INDEX ON map_${wiki}_new (title);
CREATE INDEX ON map_${wiki}_new (categories);
CREATE INDEX ON map_${wiki}_new USING GIST (location);
VACUUM ANALYZE map_${wiki}_new;

BEGIN;
DROP TABLE map_${wiki};
ALTER TABLE map_${wiki}_new RENAME TO map_${wiki};
COMMIT;

/* delete old tables */
DROP TABLE $wiki;
EOF
    ) 10>/scratch/map.flock

)& done

wait

# Connect-and-die to the renderd container to let it know that a data-import
# has just completed
echo "requesting renderd refresh"
echo | nc -N mapnik-service 5555
exit 0
