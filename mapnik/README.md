# Mapnik

Mapnik renders and serves tiles for Wikimap.

## Styling

In Wikimap, article markers are scaled inversely according visual density (density in map space, rather than real space). This approach produces a map where markers in low-density areas (such as the Atlantic) are larger on the map, and markers in high-density areas (such as New York City) are smaller.

This is implemented mostly in [styles.xml.j2](mapnik/mapnik/styles.xml.j2), with the expensive calculation of per-article density done in [Wikiscan](../wikiscan/wikiscan/sql.py).

### Determining visual density

The zoom level is some positive integer: $`z_i\in\mathbb{Z}^+`$.

$`\mathrm{viewspace}_z`$, or the area of our viewport, is: $`C_1 \mathrm{m^2}`$, where $`C_1`$ is some constant. $`\mathrm{viewspace}_z`$ is constant, as our imaginary viewport never changes size.

$`\mathrm{realspace}_{z_i}`$, or the area of earth displayed, is: $`\frac{C_2}{2^{2*z_i}} \mathrm{m^2}`$, where $`C_2`$ is some constant. As the zoom-level increases, $`\mathrm{realspace}_{z_i}`$ shrinks.

$`\mathrm{realdensity}(x,y)_{z_i}`$, or the article density at a given (x,y) coordinate, is: $`\mathrm{articles} / \mathrm{realspace}_{z_i}`$.

What we want to find out is the visual density at a given coordinate: $`\mathrm{articles}/\mathrm{viewspace}_{z_i}`$.

We can can derive this as:

```math
\begin{aligned}
\mathrm{visualdensity}(x,y)_{z_i} &= \mathrm{realdensity}(x,y)_{z_i} \frac{\mathrm{realspace}_{z_i}}{\mathrm{viewspace}_{z_i}}\\
&= \mathrm{realdensity}(x,y)_{z_i} \frac{\frac{C_2}{2^{2*z_i}}}{C_1}\\
&= C_3\frac{\mathrm{realdensity}(x,y)_{z_i}}{2^{2*z_i}}
\end{aligned}
```

In practice, each article marker is scaled inversely to its $`\mathrm{visualdensity}(x,y)_{z_i}`$.

## Building

```bash
podman build . -t KUBE_PREFIX/mapnik
```
