#!/usr/bin/env python3

import os
import sys

from jinja2 import Environment

if __name__ == '__main__':
    env = Environment(extensions=['jinja2.ext.do'])
    j2 = sys.stdin.read()
    template = env.from_string(j2)
    print(template.render(env=os.environ))
