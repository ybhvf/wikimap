#!/bin/bash

wikicodes=($WIKICODES)

# TODO: this is ugly
for wikicode in "${wikicodes[@]}"
do
    export WIKICODE=${wikicode}

    /etc/mapnik/jinja.py < /etc/mapnik/map.xml.j2 > /etc/mapnik/map_${wikicode}.xml
    /etc/mapnik/jinja.py < /tmp/renderd.conf.j2 >> /etc/renderd.conf
done

/etc/mapnik/jinja.py < /etc/mapnik/styles.xml.j2 > /etc/mapnik/styles.xml
