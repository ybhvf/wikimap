# PostGIS

For details on how PostGIS is deployed, see [kube.yaml](kube.yaml). The table
schema can be found in [Wikiscan](../wikiscan/wikiscan/sql.py).

# Building

```bash
podman build . -t KUBE_PREFIX/postgis
```
